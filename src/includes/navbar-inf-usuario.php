<div class="navbarInterRight pt7 col-xs-3 visible-lg wow fadeInRight" data-wow-delay="0s">
    <div>
        <div class="pd-x-0 pb2">
            <h2 class="titles-navInterR">INFORMACIÓN<br>AL USUARIO</h2>
        </div>
        <ul class="navList-InterRight">
            <li class="navItem-InterRight <?= in_array('convenios.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="convenios.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-convenios iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">CONVENIOS</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('deberes-y-derechos.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="deberes-y-derechos.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-deberes-derechos iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">DEBERES<br>Y DERECHOS</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('guia-de-hospitalizacion.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="guia-de-hospitalizacion.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-guia-hospitalizacion iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">GUÍA DE<br>HOSPITALIZACIÓN</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('gestion-de-paus.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="gestion-de-paus.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-gestion-paus iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">GESTIÓN<br>DE PAUS</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('seguridad-al-paciente.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="seguridad-al-paciente.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-seguridad-paciente iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">SEGURIDAD<br>AL PACIENTE</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('tips-de-salud.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="tips-de-salud.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-tips-salud iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">TIPS<br>DE SALUD</h2>
                </a>
            </li>
        </ul>
    </div>
</div>