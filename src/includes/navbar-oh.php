<div class="navbarInterRight pt7 col-xs-3 visible-lg wow fadeInRight" data-wow-delay="0s">
    <div>
        <div class="pd-x-0 pb2">
            <h2 class="titles-navInterR">LA CLÍNICA</h2>
        </div>
        <ul class="navList-InterRight">
            <li class="navItem-InterRight <?= in_array('nuestro-fundador.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="nuestro-fundador.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-fundador  iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Nuestro<br>fundador</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('historia-de-la-oh.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="historia-de-la-oh.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-hist-oh iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Historia<br>de la oh</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('mision-y-vision.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="mision-y-vision.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-mis-vis iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Misión<br>y visión</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('principios-y-valores.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="principios-y-valores.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-princ-valores iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Principios<br>y valores</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('estructura-organica.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="estructura-organica.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-estr-org iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Estructura<br>orgánica</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('nuestro-directorio.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="nuestro-directorio.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-directorio iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Nuestro<br>directorio</h2>
                </a>
            </li>
            <li class="navItem-InterRight <?= in_array('vocaciones-hospitalarias.php', $uriSegments ) ? 'active' : ''; ?>">
                <a class="navLink-InterRight" href="vocaciones-hospitalarias.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-world-hands iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">Vocaciones<br>hospitalarias</h2>
                </a>
            </li>
        </ul>
    </div>
</div>