<div class="navbarInterRight pt7 col-xs-3 visible-lg nav-for">
    <div>
        <div class="pd-x-0 pb2">
            <h2 class="titles-navInterR">Formación</h2>
        </div>
        <ul class="navList-InterRight">
            <li class="navItem-InterRight"><a class="navLink-InterRight" href="pastoral-de-la-salud.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-pastoral  iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">pastoral de<br>la salud y social</h2>
                </a></li>
            <li class="navItem-InterRight"><a class="navLink-InterRight" href="escuela-de-hospitalidad.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-hospitalidad iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">escuela de<br>hospitalidad</h2>
                </a></li>
            <li class="navItem-InterRight"><a class="navLink-InterRight" href="etica-y-bioetica.php">
                    <div class="navIcon-InterRight"><i class="icon-InterRight icon-etica iplomo"></i></div>
                    <h2 class="navTitle-InterRight text-uppercase p-internas">ética<br>y bioética</h2>
                </a></li>
        </ul>
    </div>
</div>