<!--MODAL-->
<section class="sct-modal sct-modal1">
    <div class="modal-content modal-content1">
        <div class="modal">
            <div class="modal-header">
                <h2 class="titles-big">POLÍTICA DE PRIVACIDAD</h2>
                <span id="modal-close-btn">&Cross;</span>
            </div>
            <div class="modal-info">
                <p class="text-internas">El sitio web de CLÍNICA SAN JUAN DE DIOS cuenta con una estricta política de privacidad y confidencialidad en la información de nuestros clientes. </p>
                <p class="text-internas">Somos responsables de todo el contenido publicado en esta página web, siendo los únicos autorizados en realizar cambios en la misma.</p>
                <p class="text-internas">Mediante la aceptación de esta política de privacidad y de protección de datos personales, usted acepta y consiente de manera expresa a CLÍNICA SAN JUAN DE DIOS, 
                    tratar los datos personales que proporciones de manera oral, escrita, o a través de cualquier medio de comunicación electrónica o convencional para los siguientes 
                    fines: Envío de publicidad, mediante cualquier medio y soporte, envío de invitaciones y actividades relacionadas a la empresa.</p>
                <p class="text-internas">Compromiso de CLÍNICA SAN JUAN DE DIOS es garantizar en salvaguardar los datos recopilados con total confidencialidad.</p>
                <p class="text-internas">El titular del dato personal o su representante podrá presentar la solicitud de ejercicio de sus derechos reconocidos en la ley 29733 escribiendo al 
                    correo de <a href="mailto:exports@innovativefoodsolutions.es" class="a-polt"><strong>comunicaciones@sanjuandediosoh.com</strong></a></p>
            </div>
        </div>
    </div>
</section>