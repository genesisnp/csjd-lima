<div class="wrapper-filter">
    <div class="col-xs-12 col-lg-5">
        <div class="selectorCustom">
            <i class="icon-estetoscopio"></i>
            <select name="servicios-especialidadesFiltro" id="servicios-especialidadesFiltro">
                <option selected>Elige un servicio o especialidad</option>
                <option value="al-inm">Alergia e inmunología</option>
                <option value="anestesiologia">Anestesiología</option>
                <option value="cardiologia">Cardiología</option>
                <option value="dermatologia">Dermatología</option>
                <option value="geriatria">Geriatría</option>
                <option value="obstetricia">Obstetricia</option>
                <option value="odontologia">Odontología</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-lg-5">
        <div class="selectorCustom">
            <i class="icon-medico"></i>
            <select name="medicoFiltro" id="medicoFiltro">
                <option selected>Encuentra a tu médico</option>
                <option value="felizOrtiz">Dr. Félix César Ortiz Herrera</option>
                <option value="socorroBedregal">Dra. Socorro Bedregal</option>
                <option value="percySanchez">Dr. Percy Sánchez Bedoya</option>
            </select>
        </div>
    </div>
    <div class=" col-xs-12 col-lg-2">
        <button class="btn-primary btn btn-bus btn-redHover shine"><span class="icon-lupa"></span> Buscar</button>
    </div>
</div>
<!--
<div class="wrapper-filter">
    <div class="dropdown col-xs-12 col-lg-5">
        <div class="select">
            <i class="icon-select icon-estetoscopio"></i>
            <span class="title-select">Elige un servicio o especialidad</span>
            <i class="icon-select icon-triangle-down"></i>
        </div>
        <input type="hidden" name="gender">
        <ul class="dropdown-menu">
            <li id="al-inm">Alergia e inmunología</li>
            <li id="anestesiologia">Anestesiología</li>
            <li id="aardiologia">Cardiología</li>
            <li id="dermatologia">Dermatología</li>
            <li id="geriatria">Geriatría</li>
            <li id="obstetricia">Obstetricia</li>
            <li id="odontologia">Odontología</li>
        </ul>
    </div>
    <div class="dropdown col-xs-12 col-lg-5">
        <div class="select">
            <i class="icon-select icon-medico"></i>
            <span class="title-select">Encuentra a tu médico</span>
            <i class="icon-select icon-triangle-down"></i>
        </div>
        <input type="hidden" name="gender">
        <ul class="dropdown-menu">
            <li id="felizOrtiz">Dr. Félix César Ortiz Herrera</li>
            <li id="socorroBedregal">Dra. Socorro Bedregal</li>
            <li id="percySanchez">Dr. Percy Sánchez Bedoya</li>
        </ul>
    </div>
    <div class=" col-xs-12 col-lg-2">
        <button class="btn-primary btn btn-bus btn-redHover shine"><span class="icon-lupa"></span> Buscar</button>
    </div>
</div>
-->